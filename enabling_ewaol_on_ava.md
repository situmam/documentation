# Enabling EWAOL on the AVA platform

## Background

[AVA Developer Platform](https://www.ipi.wiki/pages/com-hpc-altra) is
based on the Ampere Altra 32-core neoverse N1 (Armv8.2) SoC. It is
used as the reference hardware for
[EWAOL](https://ewaol.docs.arm.com/en/v1.0/introduction.html), which
is the software reference implementation of [SOAFEE](https://soafee.io/)

## Prerequisites

Note: if you don’t have a VGA display you will need to purchase a VGA
to HDMI converter to interact with the firmware via a graphical
display.

Alternatively, you can use UART in order to interact with the AVA
board. Programs such as `minicom` or `telnet` can be used to connect
with the target in order to select a boot device via the UEFI
menu. 

Below are instructions on how to use `telnet`.

Install ser2net (Serial to network proxy) application on Ubuntu/Debian:
   ```console
   $ sudo apt-get install ser2net
   ```

Append one line for ser2net configuration file. On your platform, it may appear as a different tty
port other than `ttyUSB2`:
   ```console
   $ sudo echo "2004:telnet:600:/dev/ttyUSB2:115200 8DATABITS NONE 1STOPBIT banner" >> /etc/ser2net.conf
   ```

Restart the ser2net service and use telnet to connect the serial port (UART):
   ```console
   $ sudo /etc/init.d/ser2net restart
   $ telnet localhost 2004
   ```

## EWAOL Build Instructions

[Reference](https://ewaol.docs.arm.com/en/v1.0/user_guide/reproduce.html#build)

Install dependencies on Ubuntu/Debian:
   ```console
   $ sudo -H pip3 install --upgrade kas==3.0.2
   $ sudo apt install bmap-tools
   ```

Build bare metal images for AVA platform:
   ```console
   $ git clone -b kirkstone-dev https://gitlab.com/Linaro/ewaol/meta-ewaol-machine
   $ cd meta-ewaol-machine
   $ kas build kas/ewaol/baremetal.yml:kas/machine/avadp-xfce.yml
   ```

Note: GPU is optional

... or build virtualization images for AVA platform:
   ```console
   $ git clone -b kirkstone-dev https://gitlab.com/Linaro/ewaol/meta-ewaol-machine
   $ cd meta-ewaol-machine
   $ kas build kas/ewaol/virtualization.yml:kas/machine/avadp-xfce.yml
   ```

Note: GPU is optional

## AVA Image Installation Instructions

Step 1: Copy the bare metal AVA images onto a USB stick
   ```console
   $ cd build/tmp_baremetal/deploy/images/ava
   $ sudo bmaptool copy --bmap ewaol-baremetal-image-ava.wic.bmap ewaol-baremetal-image-ava.wic.gz /dev/sdX
   ```

... Or copy the virtualization image onto a USB stick
   ```console
   $ cd build/tmp_virtualization/deploy/images/ava
   $ sudo bmaptool copy --bmap ewaol-virtualization-image-ava.wic.bmap ewaol-virtualization-image-ava.wic.gz /dev/sdX
   ```

Note: Replace the `sdX` with the USB stick device name on your PC. You
can use `dmesg | tail -n 20` command to identify the desired USB stick device.

Step 2: Connect your host (e.g. laptop) UART port to the AVA platform. The AVA platform provides serial port (RS232 with male
pinout). You may need to use an RS232-to-USB converter to connect your host system via a USB port in which casae ttyUSB device will be avaible for telent to use.
   ```console
   $ telnet localhost 2004
   ```

Step 3: Power on the AVA platform and select the entry `Boot Manager`
from the UEFI menu, then select the desired boot device. In this case,
it's the USB stick that contains the image you build earlier. See
following reference screenshots.

   ![UEFI Boot Manager](images/ava_uefi_menu.png)
   ![UEFI GRUB Menu](images/ava_grub_menu.png)

Note 1: If the telnet session disconnects, you will need to kill the telnet process and reconnect again.

Note 2: Booting virtualization image, Linux kernel reports GIC error as below, which is a known issue which is described in [ADLINK Xen doc](https://github.com/ADLINK/meta-adlink-ampere/tree/main/dynamic-layers/meta-virtualization/recipes-extended/xen)
   ```console
   [    0.403737] ------------[ cut here ]------------
   [    0.403738] WARNING: CPU: 30 PID: 0 at drivers/irqchip/irq-gic-v3-its.c:3074 its_cpu_init+0x814/0xae0
   [    0.403745] Modules linked in:
   [    0.403748] CPU: 30 PID: 0 Comm: swapper/30 Tainted: G? ? ? ? W ? ? ? ? 5.15.23-ampere-lts-standard #1
   [    0.403752] pstate: 600001c5 (nZCv dAIF -PAN -UAO -TCO -DIT -SSBS BTYPE=--)
   [    0.403755] pc : its_cpu_init+0x814/0xae0
   [    0.403758] lr : its_cpu_init+0x810/0xae0
   [    0.403761] sp : ffff800009c03ce0
   [    0.403762] x29: ffff800009c03ce0 x28: 000000000000001e x27: ffff880711f43000
   [    0.403767] x26: ffff80000a3c0070 x25: fffffc1ffe0a4400 x24: ffff80000a3c0000
   [    0.403770] x23: ffff8000095bc998 x22: ffff8000090a6000 x21: ffff800009850cb0
   [    0.403774] x20: ffff800009701a10 x19: ffff800009701000 x18: ffffffffffffffff
   [    0.403777] x17: 3030303035303031 x16: 3030313030303078 x15: 303a30206e6f6967
   [    0.403780] x14: 6572206530312072 x13: 3030303030353030 x12: 3130303130303030
   [    0.403784] x11: 78303a30206e6f69 x10: 6765722065303120 x9 : ffff80000870e710
   [    0.403788] x8 : 6964657220646e75 x7 : 0000000000000003 x6 : 0000000000000000
   [    0.403791] x5 : 0000000000000000 x4 : fffffc0000000000 x3 : 0000000000000010
   [    0.403794] x2 : 000000000000ffff x1 : 0000000000010000 x0 : 00000000ffffffed
   [    0.403798] Call trace:
   [    0.403799]? its_cpu_init+0x814/0xae0
   [    0.403802]? gic_starting_cpu+0x48/0x90
   [    0.403805]? cpuhp_invoke_callback+0x16c/0x5b0
   [    0.403808]? cpuhp_invoke_callback_range+0x78/0xf0
   [    0.403811]? notify_cpu_starting+0xbc/0xdc
   [    0.403814]? secondary_start_kernel+0xe0/0x170
   [    0.403817]? __secondary_switched+0x94/0x98
   [    0.403821] ---[ end trace f68728a0d3053b70 ]---
   ```

## AVA Hardware revisions

Currently the AVA platform has two hardware revisions: A1 and A2. A2 revision is marked by two
black brackets and green digits with the segment display "FF" in the bottom right corner.

   ![AVA led revision](images/ava_led_revision.jpg)

Note: firmware and debug boards are hardware revision specific.

## BMC and using ipmitool (optional)

AVA platform provides baseboard management controller (BMC) in order
to control the board with a serial or ethernet port. This is helpful
for development or automated testing setups.

[IPI wiki](https://docs.ipi.wiki/COMHPC/ava/HowToChangeBMCMACIPAddress.html#Using-BMC-Serial-Console-port) provides instructions needed to connect to the BMC and interact with it. Here we list commands for setting up the tool on host and control the board via BMC with ethernet port.

By default, the BMC has a static network address equal to `192.168.0.100`.  For initial setup, connect an ethernet cable to the lowest ethernet port on the rear panel of the AVA platform to your host machine or network switch.

Step 1: Install `ipmitool` on Ubuntu/Debian.
   ```console
   sudo apt install ipmitool
   ```

Step 2: Configure your host ethernet device connected to the BMC's ethernet port (replace `eth0` with your local net device).
   ```console
   sudo ifconfig eth0 192.168.0.22 netmask 255.255.255.0
   ```
   ... Or use the command below to configure the IP address.
   ```console
   sudo ip addr add 192.168.0.101/24 dev eth0
   ```

Step 3: Lauch the `ipmitool` to control the board.

   Read board basic info:
   ```console
   ipmitool -I lanplus -H 192.168.0.100 -U admin -P admin -b 0 -t 0x82 mc info
   
   Device ID                 : 52
   Device Revision           : 0
   Firmware Revision         : 2.03
   IPMI Version              : 1.5
   Manufacturer ID           : 24339
   Manufacturer Name         : ADLINK Technology Inc.
   Product ID                : 8449 (0x2101)
   Product Name              : Unknown (0x2101)
   Device Available          : yes
   Provides Device SDRs      : yes
   Additional Device Support :
       Sensor Device
       FRU Inventory Device
       IPMB Event Generator
   Aux Firmware Rev Info     :
       0xa2
       0x03
       0x18
       0x01
   ```

   Read BMC ethernet info:
   ```console
   ipmitool -I lanplus -H 192.168.0.100 -U admin -P admin lan print 1
   
    Set in Progress         : Set Complete
   Auth Type Support       : MD5 
   Auth Type Enable        : Callback : MD5 
                           : User     : MD5 
                           : Operator : MD5 
                           : Admin    : MD5 
                           : OEM      : MD5 
   IP Address Source       : Static Address
   IP Address              : 192.168.0.100
   Subnet Mask             : 255.255.255.0
   MAC Address             : 00:30:64:3c:2c:3d
   SNMP Community String   : AMI
   IP Header               : TTL=0x40 Flags=0x40 Precedence=0x00 TOS=0x10
   BMC ARP Control         : ARP Responses Enabled, Gratuitous ARP Disabled
   Gratituous ARP Intrvl   : 0.0 seconds
   Default Gateway IP      : 192.168.0.1
   Default Gateway MAC     : 00:00:00:00:00:00
   Backup Gateway IP       : 0.0.0.0
   Backup Gateway MAC      : 00:00:00:00:00:00
   802.1q VLAN ID          : Disabled
   802.1q VLAN Priority    : 0
   RMCP+ Cipher Suites     : 0,1,2,3,6,7,8,11,12,15,16,17
   Cipher Suite Priv Max   : caaaaaaaaaaaXXX
                           :     X=Cipher Suite Unused
                           :     c=CALLBACK
                           :     u=USER
                           :     o=OPERATOR
                           :     a=ADMIN
                           :     O=OEM
   Bad Password Threshold  : 0
   Invalid password disable: no
   Attempt Count Reset Int.: 0
   User Lockout Interval   : 0
   ```

   Power on the board:
   ```console
   ipmitool -I lanplus -H 192.168.0.100 -U admin -P admin chassis power on
   Chassis Power Control: Up/On
   ```
   Power off the board:
   ```console
   ipmitool -I lanplus -H 192.168.0.100 -U admin -P admin chassis power off
   Chassis Power Control: Down/Off
   ```

   Reboot the board:
   ```console
   ipmitool -I lanplus -H 192.168.0.100 -U admin -P admin chassis power cycle
   Chassis Power Control: Cycle
   ```

## Debricking

ADLINK provides DediProg SF600 and
[DB40](https://www.adlinktech.com/Products/Computer_on_Modules/EngineeringTools/DB40_Debug_Module)
debug boards, which are platform revision specific:

 * Green DB40 is for the A1 revision
 * Black DB40 is for the A2 revision



