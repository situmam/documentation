# Open AD Kit Autonomy Blueprint

## Background

A SOAFEE SIG Blueprint is a reference implementation of a software
defined vehicle (SDV) automotive use-case. It includes an example
software stack that can be executed on a specified target platform
enabling automotive ecosystem and supply chain partners to accelerate
adoption of new SW defined products & systems leveraging the SOAFEE
architecture.

## Autonomy Smart Blueprint

The Open AD Kit Autonomy Blueprint is part of SOAFEE Release 2
(R2). It is an autonomous driving use-case example implemented using
the Autoware Foundation [Open AD
Kit](https://www.autoware.org/autoware-open-ad-kit).  This is executed
on the Edge Workload Abstraction & Orchestration Layer
([EWAOL](https://gitlab.com/Linaro/ewaol)), a SOAFEE reference
implementation. This repository maintains the documentation needed to
setup EWAOL on [ADLINK AVA Developer
Platform](https://www.adlinktech.com/Products/Computer_on_Modules/COM-HPC-Server-Carrier-and-Starter-Kit/AVA_Developer_Platform) using Autoware as a workload.  The picture below illustrates the
software stack running on the ADLINK AVA Developer platform and the
simulation and visualization tools for the use-case.

![Blueprint Layout](images/open-ad-kit-autonomy-blueprint-layout.png "Blueprint Layout")

### Build & Install EWAOL on the AVA Developer Platform

Use these [instructions](enabling_ewaol_on_ava.md) to build and
install EWAOL on the AVA Developer Platform

### Setup Open AD Kit on the AVA Developer Platform

Once you [setup Open AD Kit on the AVA platform](https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/system-setup-ava.md), you can run it using [these instruction](https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/run-autoware.md#run-autowareauto-on-ava-platform-or-pcu).

### Setup Open AD Kit on the Host System (x86)

Upon [seting up Open AD Kit on your host (x86) system](https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/system-setup-host.md), you can run it using these [instructions](https://gitlab.com/autowarefoundation/autoware_reference_design/-/blob/main/docs/Appendix/Open-AD-Kit-Start-Guide/run-autoware.md#run-visialization-and-scenario-simulator-on-your-host-machine).

