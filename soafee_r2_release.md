
# SOAFEE Reference Software Stack – R2 Release

SOAFEE R2 builds on the success of SAOFEE R1 released in Oct 2021 in
conjunction with the public launch of the SOAFEE SIG.  Key objectives
of this release are listed below:

* Introduce the latest release of Edge Workload Abstraction and
  Orchestration Layer (EWAOL), a SOAFEE open-source reference
  implementation.  EWAOL provides users with a standards based
  framework using containers for the deployment and orchestration of
  automotive applications on edge platforms.
	
* Introduce the Open AD Kit Autonomy Blueprint.  This provides a
  working example of how an autonomous drive reference workload can be
  deployed using containers.  This deploys the [Autoware Open AD
  Kit](https://www.autoware.org/autoware-open-ad-kit) on the latest
  version of EWAOL. This is intended to bootstrap and accelerate the
  development and demonstration of micro-services based architectures
  in automotive.

This is a SOAFEE SIG community driven effort under the SOAFEE SIG open
governance model. Features Supported in SOAFEE R2

* EWAOL Yocto Linux distribution based on poky.conf distro
* EWAOL system architecture support for baremetal (native Linux) and
  virtualization (Xen)
* OCI Compliant Container Engine Runtime
* K3S Container Orchestration Client
* Autoware Open AD Kit 1.0 validated on EWAOL baremetal configuration
  on ADLINK AVA Developer Platform

## Edge Workload Abstraction and Orchestration Layer (EWAOL)

EWAOL provides users with a standards-based framework using
containers/hypervisor for development/deployment of applications on
embedded edge platforms. This includes key device abstraction layers
to decouple software from underlying hardware. EWAOL forms one of the
fundamental components of SOAFEE architecture reference
implementation. 

### EWAOL Quick Start Documents

* [EWAOL Documentation](https://ewaol.docs.arm.com/en/v1.0/)
* [EWAOL Bring-up guide for ADLINK AVA Developer Platform](https://gitlab.com/situmam/documentation/-/blob/main/enabling_ewaol_on_ava.md)

### EWAOL Platform Support

* [Armv8-A Base RevC AEM FVP](https://developer.arm.com/Tools%20and%20Software/Fixed%20Virtual%20Platforms#Technical-Specifications)
* [Arm Neoverse N1 System Development Platform (N1SDP)](https://community.arm.com/oss-platforms/w/docs/458/neoverse-n1-sdp)
* [ADLINK AVA Developer Platform](https://www.ipi.wiki/pages/com-hpc-altra)
* Coming soon: [RockPi4](https://rockpi.org/rockpi4)

## Open AD Kit Autonomy Blueprint

Open AD Kit Autonomy Blueprint provides a working example of a
reference application workload running in containers deployed on the
EWAOL 1.0 release running on the ADLINK AVA Developer Platform.  Open
AD Kit is an active Autoware Foundation community project.  For the
latest information and resources please refer to the [Open AD Kit
page](https://www.autoware.org/autoware-open-ad-kit) on Autoware
Foundation website. Open AD Kit Blueprint details can be found
here. https://gitlab.com/Linaro/blueprints/automotive/documentation

## Feedback & Support

Join the conversation and discussions on the our [Reference
Implementation
Group](https://groups.google.com/a/soafee.io/g/reference-implementation).
